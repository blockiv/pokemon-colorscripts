clean: 
	rm -rf /opt/pokemon-colorscripts

install: clean
	mkdir -p /opt/pokemon-colorscripts/colorscripts
	cp -rf colorscripts/* /opt/pokemon-colorscripts/colorscripts
	cp nameslist.txt /opt/pokemon-colorscripts
	cp pokemon-colorscripts.sh /usr/local/bin/pokemon-colorscript

uninstall:
	rm -rf /opt/pokemon-colorscripts
	rm -f /usr/local/bin/pokemon-colorscript

